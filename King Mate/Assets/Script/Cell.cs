﻿using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour
{
    public Image outline;

    [HideInInspector]
    public Vector2Int boardPos = Vector2Int.zero;
    [HideInInspector]
    public Board board = null;
    [HideInInspector]
    public RectTransform rectTrans = null;
    [HideInInspector]
    public BasePiece currPiece = null;

    public void Setup(Vector2Int newBoardPos, Board newBoard)
    {
        boardPos = newBoardPos;
        board = newBoard;
        rectTrans = GetComponent<RectTransform>();
    }

    public void RemovePiece()
    {
        if(currPiece != null)
        {
            currPiece.Kill();
        }
    }
}
