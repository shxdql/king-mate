﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Board boa;

    public PieceManager pm;

    void Start()
    {
        boa.Create();

        pm.Setup(boa);
    }
}
