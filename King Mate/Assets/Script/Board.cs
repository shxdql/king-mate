﻿using UnityEngine;
using UnityEngine.UI;

public enum CellState
{
    None,
    Friend,
    Enemy,
    Free,
    OutOfBound
}

public class Board : MonoBehaviour
{
    public GameObject cellPF;

    [HideInInspector]
    public Cell[,] allCell = new Cell[8, 8];

    public void Create()
    {
        for(int y = 0; y < 8; y++)
        {
            for(int x = 0; x < 8; x++)
            {
                GameObject newCell = Instantiate(cellPF, transform);
                RectTransform rectTransform = newCell.GetComponent<RectTransform>();
                rectTransform.anchoredPosition = new Vector2((x * 100) + 50, (y * 100) + 50);
                allCell[x, y] = newCell.GetComponent<Cell>();
                allCell[x, y].Setup(new Vector2Int(x, y), this);
            }
        }

        for(int x = 0; x < 8; x += 2)
        {
            for(int y = 0; y < 8; y++)
            {
                int offset = (y % 2 != 0) ? 0 : 1;
                int finalX = x + offset;
                allCell[finalX, y].GetComponent<Image>().color = new Color32(251, 140, 126, 255);
            }
        }
    }

    public CellState ValidateCell(int targetX, int targetY, BasePiece CheckingPiece)
    {
        if (targetX < 0 || targetX > 7)
            return CellState.OutOfBound;
        if (targetY < 0 || targetY > 7)
            return CellState.OutOfBound;

        Cell target = allCell[targetX, targetY];

        if(target.currPiece != null)
        {
            if (CheckingPiece.clr == target.currPiece.clr)
                return CellState.Friend;
            if (CheckingPiece.clr != target.currPiece.clr)
                return CellState.Enemy;
        }

        return CellState.Free;
    }
}
