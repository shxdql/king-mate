﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public abstract class BasePiece : EventTrigger
{
    [HideInInspector]
    public Color clr = Color.clear;

    protected Cell ori = null;
    protected Cell current = null;

    protected RectTransform rectTrans = null;
    protected PieceManager pm;

    protected Cell target = null;

    protected Vector3Int move = Vector3Int.one;
    protected List<Cell> highlightCell = new List<Cell>();

    public virtual void Setup(Color newColor, Color32 newSpriteColor, PieceManager newPM)
    {
        pm = newPM;

        clr = newColor;
        GetComponent<Image>().color = newSpriteColor;
        rectTrans = GetComponent<RectTransform>();
    }

    public void Place(Cell newCell)
    {
        current = newCell;
        ori = newCell;
        current.currPiece = this;

        transform.position = newCell.transform.position;
        gameObject.SetActive(true);
    }

    public virtual void Reset()
    {
        Kill();

        Place(ori);
    }

    public virtual void Kill()
    {
        current.currPiece = null;

        gameObject.SetActive(false);
    }

    private void CreateCellPath(int x, int y, int movement)
    {
        int currX = current.boardPos.x;
        int currY = current.boardPos.y;

        for(int i = 1; i <= movement; i++)
        {
            currX += x;
            currY += y;

            CellState cs = CellState.None;
            cs = current.board.ValidateCell(currX, currY, this);

            if(cs == CellState.Enemy)
            {
                highlightCell.Add(current.board.allCell[currX, currY]);
                break;
            }

            if (cs != CellState.Free)
                break;

            highlightCell.Add(current.board.allCell[currX, currY]);
        }
    }

    protected virtual void CheckPath()
    {
        CreateCellPath(1, 0, move.x);
        CreateCellPath(-1, 0, move.x);

        CreateCellPath(0, 1, move.y);
        CreateCellPath(0, -1, move.y);

        CreateCellPath(1, 1, move.z);
        CreateCellPath(-1, 1, move.z);

        CreateCellPath(-1, -1, move.z);
        CreateCellPath(1, -1, move.z);
    }

    protected void ShowCells()
    {
        foreach (Cell cell in highlightCell)
            cell.outline.enabled = true;
    }

    protected void ClearCells()
    {
        foreach (Cell cell in highlightCell)
            cell.outline.enabled = false;

        highlightCell.Clear();
    }

    protected virtual void Move()
    {
        target.RemovePiece();

        current.currPiece = null;

        current = target;
        current.currPiece = this;

        transform.position = current.transform.position;
        target = null;
    }

    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);

        CheckPath();

        ShowCells();
    }

    public override void OnDrag(PointerEventData eventData)
    {
        base.OnDrag(eventData);

        transform.position += (Vector3)eventData.delta;

        foreach(Cell cell in highlightCell)
        {
            if(RectTransformUtility.RectangleContainsScreenPoint(cell.rectTrans,Input.mousePosition))
            {
                target = cell;
                break;
            }

            target = null;
        }
    }

    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);

        ClearCells();

        if(!target)
        {
            transform.position = current.gameObject.transform.position;
            return;
        }

        Move();

        pm.SwitchSide(clr);
    }
}
