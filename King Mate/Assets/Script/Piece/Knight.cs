﻿using UnityEngine.UI;
using UnityEngine;

public class Knight : BasePiece
{
    public override void Setup(Color newColor, Color32 newSpriteColor, PieceManager newPM)
    {
        base.Setup(newColor, newSpriteColor, newPM);

        GetComponent<Image>().sprite = Resources.Load<Sprite>("T_Knight");
    }

    private void CreateCellPath(int f)
    {
        int currX = current.boardPos.x;
        int currY = current.boardPos.y;

        MatchesState(currX - 2, currY + (1 * f));
        MatchesState(currX - 1, currY + (2 * f));
        MatchesState(currX + 1, currY + (2 * f));
        MatchesState(currX + 2, currY + (1 * f));
    }

    protected override void CheckPath()
    {
        CreateCellPath(1);
        CreateCellPath(-1);
    }

    private void MatchesState(int targetX, int targetY)
    {
        CellState cs = CellState.None;
        cs = current.board.ValidateCell(targetX, targetY, this);

        if (cs != CellState.Friend && cs != CellState.OutOfBound)
            highlightCell.Add(current.board.allCell[targetX, targetY]);
    }
}
