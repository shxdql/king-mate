﻿using UnityEngine.UI;
using UnityEngine;

public class Rook : BasePiece
{
    public override void Setup(Color newColor, Color32 newSpriteColor, PieceManager newPM)
    {
        base.Setup(newColor, newSpriteColor, newPM);

        move = new Vector3Int(7, 7, 0);
        GetComponent<Image>().sprite = Resources.Load<Sprite>("T_Rook");
    }
}
