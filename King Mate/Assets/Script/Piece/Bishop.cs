﻿using UnityEngine.UI;
using UnityEngine;

public class Bishop : BasePiece
{
    public override void Setup(Color newColor, Color32 newSpriteColor, PieceManager newPM)
    {
        base.Setup(newColor, newSpriteColor, newPM);

        move = new Vector3Int(0, 0, 7);
        GetComponent<Image>().sprite = Resources.Load<Sprite>("T_Bishop");
    }
}
