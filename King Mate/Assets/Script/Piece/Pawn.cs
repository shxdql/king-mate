﻿using UnityEngine.UI;
using UnityEngine;

public class Pawn : BasePiece
{
    private bool firstMove = true;

    public override void Setup(Color newColor, Color32 newSpriteColor, PieceManager newPM)
    {
        base.Setup(newColor, newSpriteColor, newPM);

        firstMove = true;

        move = clr == Color.white ? new Vector3Int(0, 1, 1) : new Vector3Int(0, -1, -1);
        GetComponent<Image>().sprite = Resources.Load<Sprite>("T_Pawn");
    }

    protected override void Move()
    {
        base.Move();

        firstMove = false;

        CheckPromotion();
    }

    private bool MatchesState(int targetX, int targetY, CellState target)
    {
        CellState cs = CellState.None;
        cs = current.board.ValidateCell(targetX, targetY, this);

        if(cs == target)
        {
            highlightCell.Add(current.board.allCell[targetX, targetY]);
            return true;
        }

        return false;
    }

    protected override void CheckPath()
    {
        int currX = current.boardPos.x;
        int currY = current.boardPos.y;

        MatchesState(currX - move.z, currY + move.z, CellState.Enemy);

        if(MatchesState(currX,currY+move.y,CellState.Free))
        {
            if(firstMove)
            {
                MatchesState(currX, currY + (move.y * 2), CellState.Free);
            }
        }

        MatchesState(currX + move.z, currY + move.z, CellState.Enemy);
    }

    public override void Reset()
    {
        base.Reset();

        firstMove = true;
    }

    private void CheckPromotion()
    {
        int currX = current.boardPos.x;
        int currY = current.boardPos.y;

        CellState cs = current.board.ValidateCell(currX, currY + move.y, this);

        if(cs == CellState.OutOfBound)
        {
            Color sprite = GetComponent<Image>().color;
            pm.PromotePiece(this, current, clr, sprite);
        }
    }
}
