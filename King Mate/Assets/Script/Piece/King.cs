﻿using UnityEngine.UI;
using UnityEngine;

public class King : BasePiece
{
    public override void Setup(Color newColor, Color32 newSpriteColor, PieceManager newPM)
    {
        base.Setup(newColor, newSpriteColor, newPM);

        move = new Vector3Int(1, 1, 1);
        GetComponent<Image>().sprite = Resources.Load<Sprite>("T_King");
    }

    public override void Kill()
    {
        base.Kill();

        pm.isKingAlive = false;
    }
}
