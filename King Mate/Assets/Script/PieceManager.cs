﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PieceManager : MonoBehaviour
{
    [HideInInspector]
    public bool isKingAlive = true;

    public GameObject piecePF;

    private List<BasePiece> white = null;
    private List<BasePiece> black = null;
    private List<BasePiece> promote = new List<BasePiece>();

    private string[] pieceOrder = new string[16]
    {
        "P", "P", "P", "P", "P", "P", "P", "P",
        "P", "P", "P", "P", "K", "P", "P", "P"
    };

    private string[] pieceOrderW = new string[8]
    {
        "R", "KN", "B", "Q", "K", "B", "KN", "R"
    };

    private Dictionary<string, Type> pieceLibrary = new Dictionary<string, Type>()
    {
        {"P",typeof (Pawn)}, {"K",typeof(King)}, {"Q",typeof(Queen)},
        {"B",typeof (Bishop)}, {"KN",typeof(Knight)}, {"R",typeof(Rook)}
    };

    public void Setup(Board board)
    {
        white = CreatePieceW(Color.white, new Color32(0, 255, 255, 255), board);
        black = CreatePiece(Color.black, new Color32(0, 0, 255, 255), board);

        PlacePieceW(0, white, board);
        PlacePiece(6, 7, black, board);

        SwitchSide(Color.black);
    }

    private List<BasePiece> CreatePiece(Color team,Color32 spriteColor,Board board)
    {
        List<BasePiece> newPiece = new List<BasePiece>();

        for(int i = 0; i < pieceOrder.Length; i++)
        {
            string key = pieceOrder[i];
            Type pieceType = pieceLibrary[key];

            BasePiece newP = CreatePieces(pieceType);
            newPiece.Add(newP);

            newP.Setup(team, spriteColor, this);
        }

        return newPiece;
    }

    private List<BasePiece> CreatePieceW(Color team,Color32 sprite, Board board)
    {
        List<BasePiece> newPiece = new List<BasePiece>();

        for(int i = 0; i < pieceOrderW.Length; i++)
        {
            string key = pieceOrderW[i];
            Type piece = pieceLibrary[key];

            BasePiece newP = CreatePieces(piece);
            newPiece.Add(newP);

            newP.Setup(team, sprite, this);
        }

        return newPiece;
    }

    private BasePiece CreatePieces(Type piece)
    {
        GameObject newPieceObject = Instantiate(piecePF);
        newPieceObject.transform.SetParent(transform);

        newPieceObject.transform.localScale = new Vector3(1, 1, 1);
        newPieceObject.transform.localRotation = Quaternion.identity;

        BasePiece newP = (BasePiece)newPieceObject.AddComponent(piece);

        return newP;
    }

    private void PlacePiece(int pawnRow, int royaltyRow, List<BasePiece> bp, Board board)
    {
        for(int i = 0; i < 8; i++)
        {
            bp[i].Place(board.allCell[i, pawnRow]);
            bp[i + 8].Place(board.allCell[i, royaltyRow]);
        }
    }

    private void PlacePieceW(int royalty, List<BasePiece> bp, Board board)
    {
        for (int i = 0; i < 8; i++)
            bp[i].Place(board.allCell[i, royalty]);
    }

    private void SetInteractive(List<BasePiece> bp, bool value)
    {
        foreach (BasePiece piece in bp)
            piece.enabled = value;
    }

    public void SwitchSide(Color color)
    {
        if(!isKingAlive)
        {
            ResetPiece();

            isKingAlive = true;

            color = Color.black;
        }

        bool isBlackTurn = color == Color.white ? true : false;

        SetInteractive(white, !isBlackTurn);
        SetInteractive(black, isBlackTurn);

        foreach(BasePiece bp in promote)
        {
            bool isBlackPiece = bp.clr != Color.white ? true : false;
            bool isPartOfTeam = isBlackPiece == true ? isBlackTurn : !isBlackTurn;

            bp.enabled = isPartOfTeam;
        }
    }

    public void ResetPiece()
    {
        foreach(BasePiece bp in promote)
        {
            bp.Kill();
            Destroy(bp.gameObject);
        }
        foreach (BasePiece piece in white)
            piece.Reset();
        foreach (BasePiece piece in black)
            piece.Reset();
    }

    public void PromotePiece(Pawn p, Cell c, Color team, Color sprite)
    {
        p.Kill();

        BasePiece promoted = CreatePieces(typeof(Queen));
        promoted.Setup(team, sprite, this);

        promoted.Place(c);

        promote.Add(promoted);
    }
}
